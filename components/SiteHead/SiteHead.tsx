import { Button, withStyles } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Link from 'next/link';
import React, { Component } from 'react';
import { connect } from 'react-redux';

const styles = {
  root: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    paddingTop: 20,
    paddingBottom: 20,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    color: '#fff'
  },
  logo: {
    height: 35
  }
};

class SiteHead extends Component {
  constructor(public props) {
    super(props);
  }

  render() {
    const { classes } = this.props;
    const links = [
      {
        href: '/',
        text: 'Home'
      },
      {
        href: '/about',
        text: 'About'
      },
      {
        href: '/products',
        text: 'Products'
      },
    ];

    const ButtonLink = ({ className, href, hrefAs, children }) => (
      <Link href={href} as={hrefAs} prefetch>
        <a className={className}>
          {children}
        </a>
      </Link>
    );

    const menuLinks = links.map((link, index) => {
      return <Button key={index} component={ButtonLink} href={link.href}
                     className={classes.menuButton}>{link.text}</Button>;
    });

    return (
      <header>
        <nav>
          <AppBar position="static" className={classes.root}>
            <img src={'/static/images/logo.png'} className={classes.logo}/>
            {menuLinks}
          </AppBar>
        </nav>
      </header>
    );
  }
}

// @ts-ignore
export default withStyles(styles)(SiteHead);
