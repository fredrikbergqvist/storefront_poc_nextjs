import { Divider, Typography, withStyles } from '@material-ui/core';
import React, { Component } from 'react';
import { connect } from 'react-redux';

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 3,
  },
  divider: {
    marginBottom: theme.spacing.unit * 3
  }
});

class SiteFooter extends Component {

  constructor(public props) {
    super(props);
  }

  render() {
    const { classes } = this.props;
    return (
      <footer className={classes.root}>
        <Divider className={classes.divider}/>
        <Typography gutterBottom>
          Copyright © 2019 Åhléns
        </Typography>
      </footer>
    );
  }
}

export default withStyles(styles)(SiteFooter);
