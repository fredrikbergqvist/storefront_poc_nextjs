import { Button } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { decrementCount, incrementCount, resetCount } from '../../core/store';

class Counter extends Component {
  constructor(public props) {
    super(props);
  }
  increment = () => {
    const { dispatch } = this.props;
    dispatch(incrementCount());
  };

  decrement = () => {
    const { dispatch } = this.props;
    dispatch(decrementCount());
  };

  reset = () => {
    const { dispatch } = this.props;
    dispatch(resetCount());
  };

  render() {
    const { count } = this.props;
    return (
      <div>
        <Typography variant="h4" gutterBottom>
          Redux!
        </Typography>
        <Typography gutterBottom>
          Count: <span>{count}</span>
        </Typography>
        <Button variant="contained" color="primary" onClick={this.increment}>+1</Button>
        <Button variant="outlined" color="secondary" onClick={this.reset}>reset</Button>
        <Button variant="contained" color="primary" onClick={this.decrement}>-1</Button>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { count } = state;
  return { count };
}

export default connect(mapStateToProps)(Counter);
