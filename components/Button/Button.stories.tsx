import Button from '@material-ui/core/Button';
import { storiesOf } from '@storybook/react';
import * as React from 'react';

storiesOf('Button', module).add('Default with text', () => {
  return (
    <Button variant="contained" color="primary">
      Hello World
    </Button>
  );
});

storiesOf('Button', module).add('Primary with text', () => {
  return (<Button variant="contained" color="secondary">
    Hello World
  </Button>);
});

