import Button, { ButtonProps } from '@material-ui/core/Button';
import withStyles from '@material-ui/core/styles/withStyles';
import * as React from 'react';
import { Component } from 'react';

const styles = {
  root: {
    borderRadius: 0,
    boxShadow: 'none'
  },
};

export interface IAhButtonProps extends ButtonProps {
  classes: {
    root: string;
  };
}

class AhButton extends Component<IAhButtonProps> {

  render() {
    const { classes, children } = this.props;
    return (<Button {...this.props} className={classes.root}>
      {children}
    </Button>);
  }
}

export default withStyles(styles)(AhButton);
