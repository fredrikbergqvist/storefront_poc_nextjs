import { storiesOf } from '@storybook/react';
import * as React from 'react';
import AhButton from './AhButton';

storiesOf('AhButton', module).add('Default with text', () => {
  return (
    <AhButton variant="contained" color="primary">
      Hello World
    </AhButton>
  );
});

storiesOf('AhButton', module).add('Primary with text', () => {
  return (<AhButton variant="contained" color="secondary">
    Hello World
  </AhButton>);
});

