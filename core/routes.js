const routes = require('next-routes');

module.exports = routes()
  .add('index', '/')
  .add('about', '/about')
  .add('products', '/products')
  .add('product', '/product/:showId')
  .add('notfound', '/*');
