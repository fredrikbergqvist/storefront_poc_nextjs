import colors from './colors';
import margins from './margins';

export default {
  colors,
  margins
};
