import { createMuiTheme } from '@material-ui/core/styles';
import createPalette from '@material-ui/core/styles/createPalette';
import colors from '../colors';

const AhlensTheme = createMuiTheme({
  palette: createPalette({
    primary: {
      light: colors.red.light,
      main: colors.red.main,
      dark: colors.red.dark,
    },
    secondary: {
      light: colors.black.light,
      main: colors.black.main,
      dark: colors.black.dark,
    }
  }),
  typography: {
    useNextVariants: true,
    fontSize: 16,
    fontFamily: 'Apercu,Arial,sans-serif'
  },
});

export default AhlensTheme;
