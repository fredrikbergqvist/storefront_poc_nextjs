export default {
  trueWhite: '#fff',
  white: '#f1f1f1',
  trueBlack: '#000',
  black: {
    main: '#000',
    light: '#d7d7d7',
    dark: '#3e3e3e'
  },
  red: {
    main: '#df1b12',
    light: '#f19391',
    dark: '#bf3030'
  }
};
