export const sizes = {
  base: '16px',
  small: '12px',
  large: '32px',
  xl: '48px'
};
