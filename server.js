const next = require('next');
const routes = require('./core/routes');
const express = require('express');
const compression = require('compression');

const app = next({dev: process.env.NODE_ENV !== 'production'});
app.prepare().then(() => {
  const server = express();
  const handler = routes.getRequestHandler(app);
  server
    .use(handler)
    .use(compression())
    .get('/product/:showId', (req, res) => {
      const actualPage = '/product';
      const queryParams = Object.assign({}, req.params, req.query);
      app.render(req, res, actualPage, queryParams);
    })
    .listen(3000);
});
