import * as React from 'react';
import SiteFooter from '../components/Footer/SiteFooter.jsx';
import SiteHead from '../components/SiteHead/SiteHead';

class BaseLayout extends React.Component {
  render() {
    const { children } = this.props;
    return <div className='layout'>
      <SiteHead/>
      {children}
      <SiteFooter/>
    </div>;
  }
}

export default BaseLayout;
