import { MuiThemeProviderProps } from '@material-ui/core/styles/MuiThemeProvider';
import Document, { Head, Main, NextScript } from 'next/document';
import * as React from 'react';
import flush from 'styled-jsx/server';
import { IPageContext } from '../core/getPageContext';

export interface IDocumentProps {
  styleTags: any
  pageContext: MuiThemeProviderProps
}

export default class MyDocument extends Document<IDocumentProps> {
  static getInitialProps({ renderPage }) {
    let pageContext: IPageContext | undefined;
    const page = renderPage((Component) => {
      const WrappedComponent = props => {
        pageContext = props.pageContext;
        return <Component {...props} />;
      };

      return WrappedComponent;
    });

    let css;
    // It might be undefined, e.g. after an error.
    if (pageContext) {
      css = (pageContext).sheetsRegistry.toString();
    }

    return {
      ...page,
      pageContext,
      // Styles fragment is rendered after the app and page rendering finish.
      styles: (
        <React.Fragment>
          <style
            id="jss-server-side"
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{ __html: css as string }}
          />
          {flush() || null}
        </React.Fragment>
      ),
    };

  }

  render() {
    const { pageContext } = this.props;

    const theme =
      typeof pageContext.theme === 'function' ? pageContext.theme(null) : pageContext.theme;
    const themeColor = theme.palette.primary.main;

    return (
      <html lang="en" dir="ltr">
      <Head>
        <meta charSet="utf-8"/>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
        />
        <meta name="theme-color" content={themeColor}/>

        <link rel="preload" href="/static/fonts/Apercu.woff2" as="font" type="font/woff2"/>
      </Head>
      <body>
      <Main/>
      <NextScript/>
      </body>
      </html>
    );
  }
}
