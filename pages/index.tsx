import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Head from 'next/head';
import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Examples from '../components/redux/examples';
import { serverRenderClock, startClock } from '../core/store';

const styles = theme => ({
  root: {
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,
    paddingTop: theme.spacing.unit * 5,
  },
});

class Index extends React.Component {
  private classes: any;
  private timer: any;

  constructor(props) {
    super(props);
    this.classes = props.classes;
  }

  static getInitialProps({ reduxStore, req }) {
    const isServer = !!req;
    reduxStore.dispatch(serverRenderClock(isServer));

    return {};
  }

  componentDidMount() {
    // @ts-ignore
    const { dispatch } = this.props;
    this.timer = startClock(dispatch);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    return (
      <div className={this.classes.root}>
        <Head>
          <title>Welcome to Åhlens</title>
        </Head>

        <div>
          <Typography variant="h4" gutterBottom>
            Welcome!
          </Typography>
          <Typography gutterBottom>
            Familjeföretaget Åhléns grundades 1899 i Insjön och är idag ett av Sveriges starkaste varumärken, med 61
            varuhus i Sverige och e-handel via åhlens.se
          </Typography>
          <Examples/>
        </div>
      </div>);
  };
}

// @ts-ignore
export default compose(
  withStyles(styles),
  connect(),
)(Index);
