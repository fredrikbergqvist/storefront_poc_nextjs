import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import fetch from 'isomorphic-unfetch';
import Head from 'next/head';
import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

const routes = require('../core/routes');

const { Link } = routes;

const styles = theme => ({
  root: {
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,
    paddingTop: theme.spacing.unit * 5,
  },
});

class Product extends React.Component {
  private classes: any;
  private readonly show: any;

  constructor(props) {
    super(props);
    this.classes = props.classes;
    this.show = props.show || {
      name: '',
      image: {
        medium: ''
      },
      summary: ''
    };
  }

  static async getInitialProps({ query }) {
    const { showId } = query;
    let show;
    if (showId) {
      const res = await fetch(`https://api.tvmaze.com/shows/${showId}`);
      show = await res.json();
    }

    return { show };
  }

  render() {
    const show = (
      <div className={this.classes.root}>
        <Head>
          <title>{this.show.name}</title>
        </Head>

        <div>
          <Typography variant="h4" gutterBottom>
            {this.show.name}
          </Typography>
          <Typography gutterBottom>
            <img src={this.show.image.medium}/>
          </Typography>
          <Typography gutterBottom>
            {this.show.summary.replace(/<[/]?p>/g, '').replace(/<[/]?b>/g, '')}
          </Typography>
          <Typography gutterBottom>
            <Link route={'products'}>
              <a>Back to list</a>
            </Link>
          </Typography>
        </div>
      </div>);
    return this.show ? show : null;
  };
}

// @ts-ignore
export default compose(
  withStyles(styles),
  connect(),
)(Product);
