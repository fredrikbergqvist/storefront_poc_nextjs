import { CssBaseline } from '@material-ui/core';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import App, { Container } from 'next/app';
import Head from 'next/head';
import React from 'react';
import JssProvider from 'react-jss/lib/JssProvider';
import { Provider } from 'react-redux';
import getPageContext from '../core/getPageContext';
import withReduxStore from '../core/with-redux-store';
import BaseLayout from '../layout/BaseLayout';
import '../styles/base.scss';

class MyApp extends App {
  private readonly pageContext;

  constructor() {
    // @ts-ignore
    super();
    this.pageContext = getPageContext();
  }

  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  componentDidMount() {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    // @ts-ignore
    const { Component, pageProps, reduxStore } = this.props;
    return <Container>
      <Provider store={reduxStore}>
        <Head>
          <title>Welcome to Åhlens</title>
        </Head>
        <JssProvider
          registry={this.pageContext.sheetsRegistry}
          generateClassName={this.pageContext.generateClassName}
        >
          <MuiThemeProvider
            theme={this.pageContext.theme}
            sheetsManager={this.pageContext.sheetsManager}
          >
            <CssBaseline/>
            <BaseLayout>
              <Component pageContext={this.pageContext} {...pageProps} />
            </BaseLayout>
          </MuiThemeProvider>
        </JssProvider>
      </Provider>
    </Container>;
  }
}

export default withReduxStore(MyApp);
