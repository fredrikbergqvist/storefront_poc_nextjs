import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Head from 'next/head';
import Link from 'next/link';
import React from 'react';
import AhButton from '../components/AhButton/AhButton';

const styles = theme => ({
  root: {
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,
    paddingTop: theme.spacing.unit * 5,
  },
});
const About = (props) => {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <Head>
        <title>About Åhlens</title>
      </Head>

      <div>
        <Typography variant="h4" gutterBottom>
          About Åhlens
        </Typography>
        <Typography variant="subtitle1" gutterBottom>
          About page
        </Typography>
        <Typography gutterBottom>
          Familjeföretaget Åhléns grundades 1899 i Insjön och är idag ett av Sveriges starkaste varumärken, med 61
          varuhus
          i Sverige och e-handel via åhlens.se
        </Typography>
        <Typography gutterBottom>
          Vi är varuhuset som erbjuder en smart mix av prisvärda produkter på ett
          inspirerande, enkelt och lättillgängligt sätt. Åhléns omsätter 4,8 miljarder och varje år tar våra 3 000
          skickliga
          medarbetare emot ca 80 miljoner besök. Åhléns är en del av detaljhandelskoncernen Axel Johnson AB.
        </Typography>
        <Typography gutterBottom>
          <Link href="/">
            <a>Go to the main page</a>
          </Link>
        </Typography>
        <AhButton variant="contained" color="primary">
          Do nothing button
        </AhButton>
        <AhButton variant="contained" color="secondary">
          Another button
        </AhButton>
      </div>
    </div>);
};

export default withStyles(styles)(About);


