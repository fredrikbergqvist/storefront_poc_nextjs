import { withStyles } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import fetch from 'isomorphic-unfetch';
import Head from 'next/head';
import Link from 'next/link';
import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

const styles = theme => ({
  root: {
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,
    paddingTop: theme.spacing.unit * 5,
  },
});

class Products extends React.Component {
  private classes: any;
  private shows: any[];

  constructor(props) {
    super(props);
    this.classes = props.classes;
    this.shows = props.shows || [];
  }

  static async getInitialProps() {
    const res = await fetch('https://api.tvmaze.com/search/shows?q=batman');
    const data = await res.json();

    return {
      shows: data
    };
  }

  render() {
    return (
      <div className={this.classes.root}>
        <Head>
          <title>Batman</title>
        </Head>

        <div>
          <Typography variant="h4" gutterBottom>
            List of Batman shows
          </Typography>
          <List>
            {this.shows.map(({ show }) => (
              <ListItem key={show.id}>
                <ListItemText>
                  <Link prefetch as={`/product/${show.id}`} href={{ pathname: 'product', query: { showId: show.id } }}>
                    <a>{show.name}</a>
                  </Link>
                </ListItemText>
              </ListItem>
            ))}
          </List>
        </div>
      </div>);
  };
}

// @ts-ignore
export default compose(
  withStyles(styles),
  connect(),
)(Products);
